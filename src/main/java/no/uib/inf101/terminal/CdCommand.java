package no.uib.inf101.terminal;

public class CdCommand implements Command {
    private Context context;

    public CdCommand(Context context) {
        this.context = context;
    }

    @Override
    public String run(String[] args) {
        if (args.length == 0) {
            context.goToHome();
            return "";
        } else if (args.length > 1) {
            return "cd: too many arguments";
        }
        String path = args[0];
        if (context.goToPath(path)) {
            return "";
        } else {
            return "cd: no such file or directory: " + path;
        }
    }

    @Override
    public String getName() {
        return "cd";
    }
}

