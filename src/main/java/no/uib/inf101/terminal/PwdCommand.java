package no.uib.inf101.terminal;

public class PwdCommand implements Command {
    private Context context;

    public PwdCommand(Context context) {
        this.context = context;
    }

    @Override
    public String run(String[] args) {
        return context.getCwd().getAbsolutePath();
    }

    @Override
    public String getName() {
        return "pwd";
    }
}
