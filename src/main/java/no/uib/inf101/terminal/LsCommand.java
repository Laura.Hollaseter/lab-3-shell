package no.uib.inf101.terminal;

import java.io.File;

public class LsCommand implements Command {
    private Context context;

    public LsCommand(Context context) {
        this.context = context;
    }

    @Override
    public String run(String[] args) {
        File cwd = context.getCwd();
        StringBuilder s = new StringBuilder();
        for (File file : cwd.listFiles()) {
            s.append(file.getName()).append(" ");
        }
        return s.toString();
    }

    @Override
    public String getName() {
        return "ls";
    }
}

