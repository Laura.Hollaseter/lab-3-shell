package no.uib.inf101.terminal;

public class CmdEcho implements Command{
    private Context context; // Declare the context field here
    // Constructor that initializes the context
    public CmdEcho() {
        // Initialize context here. This could be a new Context or a default/static instance.
        // If Context cannot be instantiated without arguments, you might need to set it to null
        // or set up a default Context in another way.
        this.context = new Context(); // Assuming Context has a no-argument constructor
    }
    public CmdEcho(Context context) {
        this.context = context;
    }

    @Override
    public String run(String[] args) {
        StringBuilder result = new StringBuilder();
        for (String arg : args) {
            result.append(arg).append(" ");  // append a space after each argument
        }
        // Do not trim the result, as the test expects the trailing space
        return result.toString();
    }
    
    
    @Override
    public String getName() {
        return "echo";   
}
}
